Password Management Tool

Introduction

A password manager is a software application that is used to store and manage the passwords that a user has for various online accounts and security features. Password managers store the passwords in an encrypted format and provide secure access to all the password information with the help of a master password.

Tasks  Performed by This Tool

1)To View saved passwords securely of Different Users.

2)This Tool makes easier to manage passwords for different applications/websites/accounts with high security

3)To Generate random secure password with user defined length and size

4)To group the password accounts
